//
// Created by vladislav on 2019-01-30.
//

#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent) : QWidget(parent) {
    this->setGeometry(500, 300, 200, 100);
//    this->qr = this->frameGeometry();
//    cp = QDesktopWidget().availableGeometry().center();
//    qr.moveCenter(cp);
//    this->move(qr.topLeft());

    this->btnOpen.setText("Открыть сценарий");
    this->btnCreate.setText("Создать сценарий");

    connect(&btnOpen, SIGNAL(clicked()), this, SLOT(onBtnOpenClicked()));

    this->grid.setColumnStretch(0, 200);
    this->grid.setColumnStretch(1, 100);
    this->grid.setColumnStretch(2, 200);

    this->grid.setColumnMinimumWidth(0, 100);
    this->grid.setColumnMinimumWidth(1, 100);
    this->grid.setColumnMinimumWidth(2, 100);

    this->grid.setRowStretch(0, 100);
    this->grid.setRowStretch(1, 100);
    this->grid.setRowStretch(2, 100);
    this->grid.setRowStretch(3, 100);


    this->grid.addWidget(&this->btnOpen, 1, 1);
    this->grid.addWidget(&this->btnCreate, 2, 1);

    this->setLayout(&this->grid);

    connect(&btnCreate, SIGNAL(clicked()), this, SLOT(onBtnCreateClicked()));
}

MainWindow::~MainWindow()
{

}

void MainWindow::onBtnOpenClicked() {
    QString filepath = this->fileDialogWindow.getOpenFileName(0, "Выберите сценарий", "", "*.json");
//    qDebug() << filepath << endl;

    if (filepath != "") {
        this->workWindow = new WorkWindow(this, filepath);
        this->hide();
    }
}

void MainWindow::onBtnCreateClicked() {
    this->scenarioCreator = new ScenarioGenerator(this);
//    this->hide();
}

//void MainWindow::mousePressEvent(QMouseEvent *event) {
//    if (event->button() == Qt::LeftButton) {
//        // здесь обрабатываем левую кнопку мыши
//    }
//}
