//
// Created by vladislav on 2019-01-30.
//

#ifndef TESTUIGENERATOR_MAINWINDOW_H
#define TESTUIGENERATOR_MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QGridLayout>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QLabel>
#include <QDebug>
#include <iostream>

#include "newPageModel.h"
#include "WorkWindow.h"
#include "MyGraphicsView.h"
#include "ScenarioGenerator.h"


class MainWindow: public QWidget {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
//    void showNewWidget();

private:
    QWidget widget;
    QRect qr;
    QPoint cp;
    QGridLayout grid;
    QPushButton btnOpen;
    QPushButton btnCreate;
    QFileDialog fileDialogWindow;
    WorkWindow *workWindow;
    MyGraphicsView *graph;

    ScenarioGenerator *scenarioCreator;


private slots:
    void onBtnOpenClicked();
    void onBtnCreateClicked();

//private signals:
//    void mousePressEvent(QMouseEvent *event);
};

#endif //TESTUIGENERATOR_MAINWINDOW_H
