//
// Created by vladislav on 2019-02-04.
//

#include "MyGraphicsView.h"

MyGraphicsView::MyGraphicsView(QString filepath) {
    connect(this, &MyGraphicsView::mySignal, this, &MyGraphicsView::scrollSlot);
    connect(this, &MyGraphicsView::secondSignal, this, &MyGraphicsView::secondSlot);

    this->path_to_image = filepath;

    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->lbl = new QLabel;
    this->scene = new QGraphicsScene();
    this->img = new QPixmap(filepath);
    this->lbl->setPixmap(*this->img);

    this->setGeometry(QRect(QPoint(500, 250), img->size()));

    this->scene->setSceneRect(0,0,img->size().width(), img->size().height());
    this->ptr = this->scene->addPixmap(*this->img);

//    this->installEventFilter(this);

    this->setScene(scene);

//    this->show();
}

void MyGraphicsView::scrollSlot(QMouseEvent *event) {
    QPointF mousePointerPos = event->localPos();
//    qDebug() << mousePointerPos;

//    qreal x = mousePointerPos.x();
//    qreal y = mousePointerPos.y();
//
//    qreal centerX = (x - this->geometry().center().x()) * 1.1;
//    qreal centerY = (y - this->geometry().center().y()) * 1.1;

    QSize sizeOfCurrentPixmap = QSize(this->img->size().width(), this->img->size().height());

    QPixmap *startPixmap = new QPixmap(this->path_to_image);
    QSize sizeOfStartPixmap = startPixmap->size();

    QSize sizeOfNewPixmap = QSize(sizeOfCurrentPixmap.width() * 1.1, \
               sizeOfCurrentPixmap.height() * 1.1);

    QPixmap tempPixmap = startPixmap->scaled(sizeOfNewPixmap, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    delete(startPixmap);
    this->scene->setSceneRect(0,0,sizeOfNewPixmap.width(), sizeOfNewPixmap.height());
    delete this->img;
    this->img = new QPixmap(tempPixmap);
    this->ptr->setPixmap(*this->img);

    update();
}

void MyGraphicsView::secondSlot(QMouseEvent *event) {
//    QPointF mousePointerPos = event->localPos();
//    qDebug() << mousePointerPos;

    QSize sizeOfCurrentPixmap = QSize(this->img->size().width(), this->img->size().height());

    QPixmap *startPixmap = new QPixmap(this->path_to_image);
    QSize sizeOfStartPixmap = startPixmap->size();

    QSize sizeOfNewPixmap = QSize(sizeOfCurrentPixmap.width() / 1.1, \
               sizeOfCurrentPixmap.height() / 1.1);

    QPixmap tempPixmap = startPixmap->scaled(sizeOfNewPixmap, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    delete(startPixmap);
    this->scene->setSceneRect(0,0,sizeOfNewPixmap.width(), sizeOfNewPixmap.height());
    delete this->img;
    this->img = new QPixmap(tempPixmap);
    this->ptr->setPixmap(*this->img);

    update();
}

bool MyGraphicsView::event(QEvent *myEvent)
{
    if (myEvent->type() == QEvent::MouseButtonDblClick) {
//        qDebug() << "Got event " << myEvent->type() << endl;
        emit this->mySignal((QMouseEvent*)myEvent);
    }
    else if (myEvent->type() == QEvent::MouseButtonPress) {
        QMouseEvent *mouseEvent = (QMouseEvent*)myEvent;
        if(mouseEvent->button() == Qt::RightButton) {
            emit this->secondSignal(mouseEvent);
        }
    }

    return QGraphicsView::event(myEvent);
}

bool MyGraphicsView::eventFilter(QObject *obj, QEvent *ev) {
    if (obj == this) {
        if (ev->type() == QEvent::Wheel) {
//            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(ev);
//            qDebug() << "Ate key press" << keyEvent->key();
            qDebug() << "event filter";
            return true;
        } else {
            return false;
        }
    }
//    } else {
//        // pass the event on to the parent class
//        return QAbstractScrollArea::eventFilter(obj, ev);
//    }
//    qDebug() << ev->type();

    return false;
}
