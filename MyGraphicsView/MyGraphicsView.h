//
// Created by vladislav on 2019-02-04.
//

#ifndef TESTUIGENERATOR_MYGRAPHICSVIEW_H
#define TESTUIGENERATOR_MYGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QLabel>
#include <QGraphicsItemGroup>
#include <QGraphicsScene>
#include <QGridLayout>
#include <QDebug>
#include <QMouseEvent>
#include <QEvent>
#include <QWidget>
#include <QGraphicsPixmapItem>

class MyGraphicsView: public QGraphicsView {
    Q_OBJECT

public:
    explicit MyGraphicsView(QString filepath = "");
    ~MyGraphicsView() = default;

private:
    QGraphicsScene *scene;
    QLabel *lbl;
    QPixmap *img;
    QGraphicsPixmapItem *ptr;
    QString path_to_image;

signals:
    void mySignal(QMouseEvent *event);
    void secondSignal(QMouseEvent *event);

public slots:
    void scrollSlot(QMouseEvent *event);
    void secondSlot(QMouseEvent *event);

protected:
    bool event (QEvent *myEvent) override;
    bool eventFilter (QObject*, QEvent*) override;
};


#endif //TESTUIGENERATOR_MYGRAPHICSVIEW_H
