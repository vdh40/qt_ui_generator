//
// Created by vladislav on 2019-02-10.
//

#include "ScenarioGenerator.h"

ScenarioGenerator::ScenarioGenerator(QWidget *parent) {
    this->parentWindow = parent;
    this->setGeometry(500, 250, 700, 500);

    this->grid = new QGridLayout;

    this->grid->setColumnStretch(0, 200);
    this->grid->setColumnMinimumWidth(0, 200);
    this->grid->setColumnStretch(1, 400);
    this->grid->setColumnMinimumWidth(1, 400);

    this->leftGrid = new QGridLayout;
    this->grid->addLayout(this->leftGrid, 1, 0);
    this->leftGrid->setAlignment(Qt::AlignTop);
    this->grid->setRowStretch(0, 0);
    this->grid->setRowStretch(1, 1);

    this->btnMain = new QPushButton("Main");
    this->addPageBtn = new QPushButton("Add page");

    connect(addPageBtn, &QPushButton::clicked, this, &ScenarioGenerator::onBtnAddPageClicked);


    this->grid->addWidget(addPageBtn, 0, 0);
    this->grid->setAlignment(addPageBtn, Qt::AlignLeft);
    this->grid->setAlignment(addPageBtn, Qt::AlignTop);



    this->rightGrid = new QGridLayout;

    this->writeScenarioBtn = new QPushButton("Write scenario");
    connect(writeScenarioBtn, &QPushButton::clicked, this, &ScenarioGenerator::writeScenario);
    this->grid->addWidget(writeScenarioBtn, 2, 1);
    this->grid->setAlignment(writeScenarioBtn, Qt::AlignRight);

    this->setLayout(grid);
    this->show();
}

void ScenarioGenerator::onBtnAddPageClicked() {
    this->createWindow = new QWidget;
    this->insertedGrid = new QGridLayout;
    this->lineEditName = new QLineEdit;
    this->btnOK = new QPushButton;
    this->lblName = new QLabel;

    this->createWindow->setLayout(insertedGrid);
    this->lblName->setText("Name");
    this->btnOK->setText("OK");
    this->insertedGrid->addWidget(this->lblName, 0, 0);
    this->insertedGrid->addWidget((QWidget*)this->lineEditName, 0, 1);
    this->insertedGrid->addWidget((QWidget*)this->btnOK, 1, 0, 1, 2);
    connect(btnOK, &QPushButton::clicked, this, &ScenarioGenerator::onBtnOKClicked);

    this->createWindow->show();
}

void ScenarioGenerator::onPageClicked() {
    this->lastClickedButton = (QPushButton*)sender();
    auto *senderPointer = (QPushButton*)sender();

    QString name(senderPointer->text());

    int i = 0;
    for (i = 0; i < this->PageModel.Pages.size(); i++) {
//        qDebug() << name << " " << PageModel.Pages[i].name << " " << (PageModel.Pages[i].name == name) << endl;
        if (this->PageModel.Pages[i].name == name) {
            break;
        }
    }

    if ((this->selectedPageIndex >= 0) && (this->PageModel.Pages[this->selectedPageIndex].elements[0] != nullptr)) {
        this->PageModel.Pages[this->selectedPageIndex].elements[0]->setParent(nullptr);
        update();
    }

    this->selectedPageIndex = i;


    QLayoutItem *child;

    if ((child = this->rightGrid->takeAt(0)) != nullptr) {
        while (child != nullptr) {
            child->widget()->setParent(nullptr);
            delete child;
            child = this->rightGrid->takeAt(0);
        }

        delete(this->rightGrid);
        this->rightGrid = new QGridLayout;
        this->grid->addLayout(this->rightGrid, 1, 1);
    } else {
        this->grid->addLayout(this->rightGrid, 1, 1);
    }

    if (this->PageModel.Pages[this->selectedPageIndex].isEmpty) {
        auto *addElementBtn = new QPushButton("+");
        this->PageModel.Pages[this->selectedPageIndex].elements.append(addElementBtn);
        this->PageModel.Pages[this->selectedPageIndex].isEmpty = false;
        connect(addElementBtn, &QPushButton::clicked, this, &ScenarioGenerator::onBtnAddElementClicked);
        this->grid->addWidget((QPushButton*)this->PageModel.Pages[this->selectedPageIndex].elements[0], 0, 1);
        this->grid->setAlignment((QPushButton*)this->PageModel.Pages[this->selectedPageIndex].elements[0], Qt::AlignRight);
    }


    this->rightGrid->setAlignment(Qt::AlignTop);

    for (int j = 0; j < this->PageModel.Pages[i].elements.size(); j++) {
        if (j == 0) {
            this->grid->addWidget((QPushButton*)this->PageModel.Pages[this->selectedPageIndex].elements[0], 0, 1);
            this->grid->setAlignment((QPushButton*)this->PageModel.Pages[this->selectedPageIndex].elements[0], Qt::AlignRight);

        }
        else if (this->PageModel.Pages[i].elements[j]->metaObject()->className() != QString("MyGraphicsView")) {
            this->rightGrid->addWidget((QWidget*)PageModel.Pages[i].elements[j], j, 0);
        }
        else {
            this->rightGrid->addWidget((QGraphicsView*)PageModel.Pages[i].elements[j], j, 0);
        }
    }

}

void ScenarioGenerator::onBtnOKClicked() {
    if(this->lineEditName->text() != "") {
        auto *pageItem = new NewPageItem;

        auto *newPage = new QPushButton;
        newPage->setText(this->lineEditName->text());
        this->pages.append(newPage);
        this->leftGrid->addWidget(newPage);
        this->leftGrid->setAlignment(newPage, Qt::AlignTop);
        connect(newPage, &QPushButton::clicked, this, &ScenarioGenerator::onPageClicked);

        pageItem->name = this->lineEditName->text();
        this->PageModel.Pages.append(*pageItem);

        this->createWindow->close();
    }
}

void ScenarioGenerator::onBtnAddElementClicked() {
    this->addElementWindow = new QWidget;
    this->additionalGrid = new QGridLayout;
    this->nameLabel = new QLabel("Name");
    this->typeLabel = new QLabel("Type");
    this->textLabel = new QLabel("Text");
    this->onClickedLabel = new QLabel("onClicked");

    this->nameEdit = new QLineEdit;
    this->typeCombobox = new QComboBox;
    this->textEdit = new QLineEdit;
    this->onClickedEdit = new QLineEdit;
    this->OKbtn = new QPushButton("OK");

    this->addElementWindow->setLayout(this->additionalGrid);

    this->additionalGrid->addWidget(this->typeLabel, 0, 0);
    this->additionalGrid->addWidget(this->nameLabel, 1, 0);
    this->additionalGrid->addWidget(this->textLabel, 2, 0);
    this->additionalGrid->addWidget(this->onClickedLabel, 3, 0);

    this->typeCombobox->insertItem(0, "Button");
    this->typeCombobox->insertItem(1, "Text");
    this->typeCombobox->insertItem(2, "CheckBox");
    this->typeCombobox->insertItem(3, "TextInput");
    this->typeCombobox->insertItem(4, "Image");


    this->additionalGrid->addWidget(this->typeCombobox, 0, 1);
    this->additionalGrid->addWidget(this->nameEdit, 1, 1);
    this->additionalGrid->addWidget(this->textEdit, 2, 1);
    this->additionalGrid->addWidget(this->onClickedEdit, 3, 1);

    this->additionalGrid->addWidget(this->OKbtn, 4, 0, 1, 2);

    connect(this->OKbtn, &QPushButton::clicked, this, &ScenarioGenerator::onBtnCreateElementClicked);

    this->addElementWindow->show();
}

void ScenarioGenerator::onBtnCreateElementClicked() {
    if (this->onClickedEdit->text() != "")
        if (this->textEdit->text() != "")
            if (this->nameEdit->text() != "")
                if (this->typeCombobox->currentText() != "") {
                    ScenarioItem *newScenarioItem = new ScenarioItem;
                    newScenarioItem->onClickedString = this->onClickedEdit->text();
                    newScenarioItem->nameString = this->nameEdit->text();
                    newScenarioItem->textString = this->textEdit->text();
                    newScenarioItem->typeString = this->typeCombobox->currentText();

                    QObject *elem;
                    if (newScenarioItem->typeString == "Text") {
                        elem = new QLabel(newScenarioItem->textString);
                    } else if (newScenarioItem->typeString == "TextInput") {
                        elem = new QLineEdit(newScenarioItem->textString);
                    } else if (newScenarioItem->typeString == "Button") {
                        elem = new QPushButton(newScenarioItem->textString);
                    } else if (newScenarioItem->typeString == "Image") {
                        elem = new MyGraphicsView;
                    } else if (newScenarioItem->typeString == "CheckBox") {
                        elem = new QCheckBox(newScenarioItem->textString);
                    }

                    this->PageModel.Pages[this->selectedPageIndex].elements.append(elem);
                    this->PageModel.Pages[this->selectedPageIndex].scenario.append(*newScenarioItem);

                    this->lastClickedButton->click();
                    this->addElementWindow->close();
                }
}

void ScenarioGenerator::writeScenario() {

    QString filepath = this->fileDialogWindow.getOpenFileName(0, "Выберите сценарий", "", "*.json");
//    qDebug() << filepath << endl;

    if (filepath != "") {
        QJsonArray pagesScenario;
        for (int i = 0; i < this->PageModel.Pages.size(); i++) {
            QJsonObject onePage;

            QJsonArray elements;
            for (int j = 0; j < this->PageModel.Pages[i].elements.size() - 1; j++) {
                QJsonObject elem;
                elem.insert("onClicked", this->PageModel.Pages[i].scenario[j].onClickedString);
                elem.insert("text", this->PageModel.Pages[i].scenario[j].textString);
                elem.insert("name", this->PageModel.Pages[i].scenario[j].nameString);
                elem.insert("type", this->PageModel.Pages[i].scenario[j].typeString);
                elements.append(elem);
            }

            onePage.insert("elems", elements);
            onePage.insert("name", this->PageModel.Pages[i].name);
            pagesScenario.append(onePage);
        }

        QFile JSONfile(filepath);
        JSONfile.open(QIODevice::WriteOnly);

        auto doc = QJsonDocument(pagesScenario);
        JSONfile.write(doc.toJson().constData());

        qDebug() << doc.toJson().constData();
    }

}
