//
// Created by vladislav on 2019-02-10.
//

#ifndef TESTUIGENERATOR_SCENARIOGENERATOR_H
#define TESTUIGENERATOR_SCENARIOGENERATOR_H

#include <QWidget>
#include <QVector>
#include <QComboBox>
#include <QPushButton>
#include <QLineEdit>
#include <QCheckBox>
#include <QVariantList>
#include <QVariantMap>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFileDialog>

#include "newPageModel.h"
#include "scenarioItem.h"

class ScenarioGenerator: public QWidget {
public:
    ScenarioGenerator(QWidget *parent = nullptr);
    ~ScenarioGenerator() override = default;

private:
    QWidget *parentWindow;
    NewPageModel PageModel;
    QGridLayout *grid;
    QGridLayout *leftGrid;
    QGridLayout *rightGrid;
    QPushButton *btnMain;
    QPushButton *addPageBtn;
    QVector<QPushButton*> pages;
    QPushButton *writeScenarioBtn;
    QFileDialog fileDialogWindow;


    QWidget *createWindow;
    QGridLayout *insertedGrid;
    QLabel *lblName;
    QLineEdit *lineEditName;
    QPushButton *btnOK;

    QWidget *addElementWindow;
    QGridLayout *additionalGrid;
    QLabel *nameLabel;
    QLabel *typeLabel;
    QLabel *textLabel;
    QLabel *onClickedLabel;
    QLineEdit *nameEdit;
    QComboBox *typeCombobox;
    QLineEdit *textEdit;
    QLineEdit *onClickedEdit;
    QPushButton *OKbtn;

    QVector<ScenarioItem*> scenarioItems;

    int selectedPageIndex = -1;
    QPushButton *lastClickedButton;


public slots:
    void onBtnAddPageClicked();
    void onBtnAddElementClicked();
    void onPageClicked();
    void onBtnOKClicked();
    void onBtnCreateElementClicked();
    void writeScenario();

};

#endif //TESTUIGENERATOR_SCENARIOGENERATOR_H