//
// Created by vladislav on 2019-02-11.
//

#ifndef TESTUIGENERATOR_SCENARIOITEM_H
#define TESTUIGENERATOR_SCENARIOITEM_H

#include <QString>

struct ScenarioItem {
    ScenarioItem(): nameString(""), typeString(""), textString(""), onClickedString("") {}
    QString nameString;
    QString typeString;
    QString textString;
    QString onClickedString;
};

#endif //TESTUIGENERATOR_SCENARIOITEM_H
