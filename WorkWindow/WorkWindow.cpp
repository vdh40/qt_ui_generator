//
// Created by vladislav on 2019-01-31.
//

#include "WorkWindow.h"

WorkWindow::WorkWindow(QWidget *parent, QString filepath) {
    this->parentWindow = parent;
    this->setGeometry(500, 250, 700, 500);

    this->grid = new QGridLayout;

    this->grid->setColumnStretch(0, 200);
    this->grid->setColumnMinimumWidth(0, 200);
    this->grid->setColumnStretch(1, 400);
    this->grid->setColumnMinimumWidth(1, 400);


    PageModel.setPageModel(filepath);

    this->leftGrid = new QVBoxLayout;
    this->grid->addLayout(this->leftGrid, 0, 0);

    for (int i = 0; i < PageModel.Pages.size(); i++) {
        QPushButton *pageName = new QPushButton(PageModel.Pages[i].name);
        connect(pageName, SIGNAL(clicked()), this, SLOT(onPageClicked()));
        this->leftGrid->addWidget(pageName, i);
        if (i != 0) {
            this->leftGrid->setAlignment(this->leftGrid->takeAt(i)->widget(), Qt::AlignTop);
        }
        this->leftGrid->addStretch(1);
    }

    this->rightGrid = new QGridLayout;

    this->btnMain = new QPushButton("Main");
    this->btnPrev = new QPushButton("Prev");
    this->btnNext = new QPushButton("Next");

    connect(btnMain, SIGNAL(clicked()), this, SLOT(onMainBtnClicked()));
    connect(btnPrev, SIGNAL(clicked()), this, SLOT(onPrevBtnClicked()));
    connect(btnNext, SIGNAL(clicked()), this, SLOT(onNextBtnClicked()));

    this->grid->addWidget(btnMain, 1, 0);
    this->grid->setAlignment(btnMain, Qt::AlignLeft);
    this->grid->addWidget(btnPrev, 1, 1);
    this->grid->setAlignment(btnPrev, Qt::AlignRight);
    this->grid->addWidget(btnNext, 1, 2);


    this->setLayout(grid);
    this->show();
}

void WorkWindow::onPageClicked() {
    auto *senderPointer = (QPushButton*)sender();


    if ((senderOfSignalToOpenPage != this->btnPrev) && (senderOfSignalToOpenPage != this->btnNext)) {
        if (this->btnDeque == nullptr) {
            push_back(senderPointer);
//            qDebug() << "Pushed back!" << endl;
        }
        if (this->btnDeque->next == nullptr) {
            push_back(senderPointer);
//            qDebug() << "Pushed back!" << endl;
        }
        else {
            dequeItem *tempItem = this->btnDeque->next;
            while (tempItem->next != nullptr) {
                tempItem = tempItem->next;
                delete(tempItem->prev);
                tempItem->prev = nullptr;
            }
            delete(tempItem);
            this->btnDeque->next = nullptr;

            push_back(senderPointer);
//            qDebug() << "Pushed center" << endl;
        }
    }

    QString name(senderPointer->text());

    int i = 0;
    for (i = 0; i < this->PageModel.Pages.size(); i++) {
//        qDebug() << name << " " << PageModel.Pages[i].name << " " << (PageModel.Pages[i].name == name) << endl;
        if (PageModel.Pages[i].name == name) {
            break;
        }
    }

    QLayoutItem *child;
    if ((child = this->rightGrid->takeAt(0)) != nullptr) {
        while (child != nullptr) {
            child->widget()->setParent(nullptr);
            delete child;
            child = this->rightGrid->takeAt(0);
        }

        delete(this->rightGrid);
        this->rightGrid = new QGridLayout;
        this->grid->addLayout(this->rightGrid, 0, 1);
    } else {
        this->grid->addLayout(this->rightGrid, 0, 1);
    }

    this->rightGrid->setAlignment(Qt::AlignTop);

    for (int j = 0; j < this->PageModel.Pages[i].elements.size(); j++) {
//        qDebug() << PageModel.Pages[i].elements[j]->metaObject()->className();
        if (PageModel.Pages[i].elements[j]->metaObject()->className() != QString("MyGraphicsView")) {
            this->rightGrid->addWidget((QWidget*)PageModel.Pages[i].elements[j], j, 0);
        }
        else {
//            ((MyGraphicsView*)PageModel.Pages[i].elements[j])->show();
            this->rightGrid->addWidget((QGraphicsView*)PageModel.Pages[i].elements[j], j, 0);
        }

    }

    senderOfSignalToOpenPage = senderPointer;
}

void WorkWindow::onMainBtnClicked() {
    this->parentWindow->show();
    this->close();
}

void WorkWindow::push_back(QPushButton *btn) {
    dequeItem *newBtnInDeque = new dequeItem;

    newBtnInDeque->btn = btn;
    newBtnInDeque->prev = this->btnDeque;
    if (btnDeque != nullptr) {
        this->btnDeque->next = newBtnInDeque;
    }
    this->btnDeque = newBtnInDeque;
}

void WorkWindow::onPrevBtnClicked() {
    if (this->btnDeque->prev != nullptr) {
        this->btnDeque = this->btnDeque->prev;
        this->senderOfSignalToOpenPage = this->btnPrev;
        this->btnDeque->btn->click();
//        qDebug() << "Prev btn clicked" << endl;
    }
}

void WorkWindow::onNextBtnClicked() {
    if (this->btnDeque->next != nullptr) {
        this->btnDeque = this->btnDeque->next;
        this->senderOfSignalToOpenPage = this->btnNext;
        this->btnDeque->btn->click();
//        qDebug() << "Next btn clicked" << endl;
    }
}
