//
// Created by vladislav on 2019-01-31.
//

#ifndef TESTUIGENERATOR_WORKWINDOW_H
#define TESTUIGENERATOR_WORKWINDOW_H


#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QDebug>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>

#include "newPageModel.h"

struct dequeItem {
    dequeItem(): prev(nullptr), next(nullptr), btn(nullptr) {}
public:
    dequeItem *prev;
    dequeItem *next;
    QPushButton *btn;
};

class WorkWindow: public QWidget {
    Q_OBJECT
public:
    WorkWindow(QWidget *parent = nullptr, QString filepath = "");
    ~WorkWindow() = default;

private:
    QWidget *parentWindow;
    NewPageModel PageModel;
    QGridLayout *grid;
    QVBoxLayout *leftGrid;
    QGridLayout *rightGrid;
    QPushButton *btnMain;
    QPushButton *btnPrev;
    QPushButton *btnNext;

    QPushButton *senderOfSignalToOpenPage;

    dequeItem *btnDeque = nullptr;

private slots:
    void onPageClicked();
    void onMainBtnClicked();
    void onPrevBtnClicked();
    void onNextBtnClicked();
    void push_back(QPushButton*);
};



#endif //TESTUIGENERATOR_WORKWINDOW_H
