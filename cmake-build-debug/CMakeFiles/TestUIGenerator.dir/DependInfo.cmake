# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/vladislav/Desktop/TestUIGenerator/MainWindow/MainWindow.cpp" "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/CMakeFiles/TestUIGenerator.dir/MainWindow/MainWindow.cpp.o"
  "/Users/vladislav/Desktop/TestUIGenerator/MyGraphicsView/MyGraphicsView.cpp" "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/CMakeFiles/TestUIGenerator.dir/MyGraphicsView/MyGraphicsView.cpp.o"
  "/Users/vladislav/Desktop/TestUIGenerator/ScenarioGenerator/ScenarioGenerator.cpp" "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/CMakeFiles/TestUIGenerator.dir/ScenarioGenerator/ScenarioGenerator.cpp.o"
  "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/TestUIGenerator_autogen/mocs_compilation.cpp" "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/CMakeFiles/TestUIGenerator.dir/TestUIGenerator_autogen/mocs_compilation.cpp.o"
  "/Users/vladislav/Desktop/TestUIGenerator/WorkWindow/WorkWindow.cpp" "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/CMakeFiles/TestUIGenerator.dir/WorkWindow/WorkWindow.cpp.o"
  "/Users/vladislav/Desktop/TestUIGenerator/main.cpp" "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/CMakeFiles/TestUIGenerator.dir/main.cpp.o"
  "/Users/vladislav/Desktop/TestUIGenerator/newPageModel/newPageModel.cpp" "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/CMakeFiles/TestUIGenerator.dir/newPageModel/newPageModel.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "TestUIGenerator_autogen/include"
  "../WorkWindow"
  "../newPageModel"
  "../MainWindow"
  "../MyGraphicsView"
  "../ScenarioGenerator"
  "../ScenarioItem"
  "/usr/local/opt/qt/lib/QtWidgets.framework"
  "/usr/local/opt/qt/lib/QtWidgets.framework/Headers"
  "/usr/local/opt/qt/lib/QtGui.framework"
  "/usr/local/opt/qt/lib/QtGui.framework/Headers"
  "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/OpenGL.framework/Headers"
  "/usr/local/opt/qt/lib/QtCore.framework"
  "/usr/local/opt/qt/lib/QtCore.framework/Headers"
  "/usr/local/opt/qt/./mkspecs/macx-clang"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
