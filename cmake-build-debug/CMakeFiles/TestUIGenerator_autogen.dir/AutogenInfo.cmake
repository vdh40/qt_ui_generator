# Meta
set(AM_MULTI_CONFIG "FALSE")
set(AM_PARALLEL "2")
set(AM_VERBOSITY "")
# Directories
set(AM_CMAKE_SOURCE_DIR "/Users/vladislav/Desktop/TestUIGenerator")
set(AM_CMAKE_BINARY_DIR "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug")
set(AM_CMAKE_CURRENT_SOURCE_DIR "/Users/vladislav/Desktop/TestUIGenerator")
set(AM_CMAKE_CURRENT_BINARY_DIR "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug")
set(AM_CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE "")
set(AM_BUILD_DIR "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/TestUIGenerator_autogen")
set(AM_INCLUDE_DIR "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/TestUIGenerator_autogen/include")
# Files
set(AM_SOURCES "/Users/vladislav/Desktop/TestUIGenerator/MainWindow/MainWindow.cpp;/Users/vladislav/Desktop/TestUIGenerator/MyGraphicsView/MyGraphicsView.cpp;/Users/vladislav/Desktop/TestUIGenerator/ScenarioGenerator/ScenarioGenerator.cpp;/Users/vladislav/Desktop/TestUIGenerator/WorkWindow/WorkWindow.cpp;/Users/vladislav/Desktop/TestUIGenerator/main.cpp;/Users/vladislav/Desktop/TestUIGenerator/newPageModel/newPageModel.cpp")
set(AM_HEADERS "/Users/vladislav/Desktop/TestUIGenerator/MainWindow/MainWindow.h;/Users/vladislav/Desktop/TestUIGenerator/MyGraphicsView/MyGraphicsView.h;/Users/vladislav/Desktop/TestUIGenerator/ScenarioGenerator/ScenarioGenerator.h;/Users/vladislav/Desktop/TestUIGenerator/ScenarioItem/scenarioItem.h;/Users/vladislav/Desktop/TestUIGenerator/WorkWindow/WorkWindow.h;/Users/vladislav/Desktop/TestUIGenerator/newPageModel/newPageModel.h")
set(AM_SETTINGS_FILE "/Users/vladislav/Desktop/TestUIGenerator/cmake-build-debug/CMakeFiles/TestUIGenerator_autogen.dir/AutogenOldSettings.txt")
# Qt
set(AM_QT_VERSION_MAJOR 5)
set(AM_QT_MOC_EXECUTABLE "/usr/local/opt/qt/bin/moc")
set(AM_QT_UIC_EXECUTABLE "")
# MOC settings
set(AM_MOC_SKIP "")
set(AM_MOC_DEFINITIONS "QT_CORE_LIB;QT_GUI_LIB;QT_WIDGETS_LIB")
set(AM_MOC_INCLUDES "/Users/vladislav/Desktop/TestUIGenerator/WorkWindow;/Users/vladislav/Desktop/TestUIGenerator/newPageModel;/Users/vladislav/Desktop/TestUIGenerator/MainWindow;/Users/vladislav/Desktop/TestUIGenerator/MyGraphicsView;/Users/vladislav/Desktop/TestUIGenerator/ScenarioGenerator;/Users/vladislav/Desktop/TestUIGenerator/ScenarioItem;/usr/local/opt/qt/lib/QtWidgets.framework;/usr/local/opt/qt/lib/QtWidgets.framework/Headers;/usr/local/opt/qt/lib/QtGui.framework;/usr/local/opt/qt/lib/QtGui.framework/Headers;/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/OpenGL.framework/Headers;/usr/local/opt/qt/lib/QtCore.framework;/usr/local/opt/qt/lib/QtCore.framework/Headers;/usr/local/opt/qt/./mkspecs/macx-clang")
set(AM_MOC_OPTIONS "")
set(AM_MOC_RELAXED_MODE "")
set(AM_MOC_MACRO_NAMES "Q_OBJECT;Q_GADGET;Q_NAMESPACE")
set(AM_MOC_DEPEND_FILTERS "")
set(AM_MOC_PREDEFS_CMD "/Library/Developer/CommandLineTools/usr/bin/c++;-dM;-E;-c;/Applications/CLion.app/Contents/bin/cmake/mac/share/cmake-3.13/Modules/CMakeCXXCompilerABI.cpp")
