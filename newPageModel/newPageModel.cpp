//
// Created by vladislav on 2019-01-30.
//

#include "newPageModel.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>

NewPageModel::NewPageModel(QObject *parent)
        : QAbstractListModel(parent)
{}

int NewPageModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return mPages.size();
}

QVariant NewPageModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const PageItem page = mPages.at(index.row());

    // FIXME: Implement me!
    switch (role) {
        case NameRole:
            return QVariant(page.name);
        case QStrRole:
            return QVariant(page.qStr);
        case ErrorRole:
            return QVariant(page.error);
    }
    return QVariant();
}

QHash<int, QByteArray> NewPageModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[NameRole] = "name";
    names[QStrRole] = "qStr";
    names[ErrorRole] = "error";
    return names;
}

void NewPageModel::setPageModel(QString filename)
{
    if (mPages.size() > 0) {
        beginRemoveRows(QModelIndex(), 0, mPages.size() - 1);
        mPages.clear();
        endRemoveRows();
    }

    QFile loadFile(filename);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    if (loadDoc.isArray()){
        QJsonArray pageArray = loadDoc.array();
        for (int pageNumber = 0; pageNumber < pageArray.size(); ++pageNumber) {
            NewPageItem newPage = createPage(pageArray[pageNumber].toObject(), pageNumber);

            //            qWarning() << newPage.qStr;
            beginInsertRows(QModelIndex(), pageNumber, pageNumber);
            Pages.append(newPage);
            endInsertRows();
        }
    }
}



NewPageItem NewPageModel::createPage(const QJsonObject &pageObject, const int &pageNumber)
{
    NewPageItem newPage;

    newPage.error = isValidStringAttribute(pageObject, "name");

    if (newPage.error.isEmpty()) {
        newPage.name = pageObject["name"].toString();
    } else {
        newPage.name = "*** Страница " + QString::number(pageNumber) + " ***";
        newPage.error = strDiv + "Название страницы:" + newPage.error;
    }

    if (pageObject.contains("elems") && pageObject["elems"].isArray()){
        QJsonArray elems = pageObject["elems"].toArray();
        for (int elemNumber = 0; elemNumber < elems.size(); ++elemNumber) {
            QObject* newElem = createElement(elems[elemNumber].toObject()); ////////////////////////////////////////////////////////////////////////////////////////

            // TODO: Добавить обработчик ошибок
            newPage.elements.append(newElem);
        }
    }

    if (!newPage.error.isEmpty()){
        // TODO: Добавить обработчик ошибок
        /*newPage.error = strDiv + "*** Ошибки сценария на странице " + QString::number(pageNumber) + " ***" + newPage.error;
        QJsonObject errorText{{"text", newPage.error}};
        newPage.qStr += createText(errorText).qStr;*/
    }



    return newPage;
}

QObject* NewPageModel::createElement(const QJsonObject &elemObject)
{
    QObject *newElem = new QObject;
    // FIXME
//    newElem.error = isValidStringAttribute(elemObject, "type");
    // FIXME
//    if (!newElem.error.isEmpty()) return newElem;

    QString elemType = elemObject["type"].toString();

    if (elemType == "Text") {
        return createText(elemObject);
    } else if (elemType == "TextInput") {
        return createTextInput(elemObject);
    } else if (elemType == "Button") {
        return createButton(elemObject);
    } else if (elemType == "Image") {
        return createImage(elemObject);
    } else if (elemType == "CheckBox") {
        return createCheckBox(elemObject);
    } else {
        // FIXME
//        newElem.error = createError("Неподдерживаемое значение атрибута <type> (" + elemType + ")");
    }

    return newElem;
}

QObject* NewPageModel::createText(const QJsonObject &elemObject)
{
    QLabel *elem = new QLabel(elemObject["text"].toString());

    return elem;
}

QObject *NewPageModel::createTextInput(const QJsonObject &elemObject)
{
    QLineEdit *elem = new QLineEdit(elemObject["name"].toString());


    return elem;
}

QObject* NewPageModel::createCheckBox(const QJsonObject &elemObject)
{
    QCheckBox *elem = new QCheckBox(elemObject["name"].toString());

    return elem;
}

QObject* NewPageModel::createButton(const QJsonObject &elemObject)
{
    QPushButton *elem = new QPushButton(elemObject["text"].toString());

    return elem;
}

QObject* NewPageModel::createImage(const QJsonObject &elemObject)
{
//    QString attributes = setAttribute("source", "root.scenarioFolder + \"/img/" + elemObject["src"].toString() + "\"", false);
    MyGraphicsView *elem = new MyGraphicsView(elemObject["src"].toString());
    qDebug() << elemObject["src"].toString();

    return elem;
}



QString NewPageModel::isValidStringAttribute(const QJsonObject &elemObject, const QString &key, const bool isRequired)
{
    if (elemObject.contains(key)){
        if (!elemObject[key].isString()) return createError("Атрибут <" + key + "> должен быть строкой");
        if (elemObject[key].toString().isEmpty()) return createError("Атрибут <" + key + "> не может быть пустой строкой");
    } else if (isRequired){
        return createError("Отсутствует обязательный атрибут <" + key + ">");
    }

    return "";
}
