//
// Created by vladislav on 2019-01-30.
//

#ifndef TESTUIGENERATOR_NEWPAGEMODEL_H
#define TESTUIGENERATOR_NEWPAGEMODEL_H


#include <QAbstractListModel>
#include <QVector>
#include <QUrl>
#include <QJsonObject>

#include "MyGraphicsView.h"
#include "scenarioItem.h"

const QString strDiv = "\n_______________\n";

struct PageItem {
    PageItem(): name(""), qStr(""), error("") {}
    QString name;
    QString qStr;
    QString error;
};

struct NewPageItem {
    NewPageItem(): name(""), error(""), isEmpty(true) {}
    QVector<QObject*> elements;
    QVector<ScenarioItem> scenario;
    QString name;
    QString error;
    bool isEmpty;
};

class NewPageModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit NewPageModel(QObject *parent = nullptr);

    enum {
        NameRole = Qt::UserRole,
        QStrRole,
        ErrorRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void setPageModel(QString filename);
//    Q_INVOKABLE QString read(QUrl path);
//    Q_INVOKABLE void write(QUrl path, QString text);
    QVector<NewPageItem> Pages;

private:
    NewPageItem createPage(const QJsonObject &pageObject, const int &pageNumber);
    QObject* createElement(const QJsonObject &elemObject);

    QObject* createText(const QJsonObject &elemObject);
    QObject* createTextInput(const QJsonObject &elemObject);
    QObject* createCheckBox(const QJsonObject &elemObject);
    QObject* createButton(const QJsonObject &elemObject);
    QObject* createImage(const QJsonObject &elemObject);

    QString wrapElement(const QString &type, const QString &body) { return type + "{" + body + "}";}
    QString setAttribute(const QString &key, const QString &value, const bool isText = true, const bool semicolonIsRequired = true);
    QString createError(const QString &error){ return "\n" + error;}
    QString createBinding(const QString &name, const bool isString);
    QString isValidStringAttribute(const QJsonObject &elemObject, const QString &key, const bool isRequired = true);
    QVector<PageItem> mPages;

};

#endif //TESTUIGENERATOR_NEWPAGEMODEL_H
